﻿using System;
namespace Wbm.OAuthV2SDK.OAuths.Tencs.Models
{
    /// <summary>
    /// 标签信息实体类
    /// </summary>
    [Serializable]
    public class TencMTag 
    {
        /// <summary>
        /// 个人标签id
        /// </summary>
        public string id { set; get; }

        /// <summary>
        /// 标签名
        /// </summary>
        public string name { set; get; }

    }

}
/*
 * Author: xusion
 * Created: 2012.07.25
 * Support: http://wobumang.com
 */