﻿using System;
using Wbm.OAuthV2SDK.OAuths.Vdisks;
namespace Wbm.OAuthV2SDK.OAuths.Vdisks.Models
{
    /// <summary>
    /// 错误代码说明
    /// </summary>
    [Serializable]
    public class VdiskMError
    {
        /// <summary>
        /// 错误码
        /// </summary>
        public int error_code { set; get; }

        /// <summary>
        /// 错误码
        /// </summary>
        public int error_detail_code { set; get; }

        /// <summary>
        ///  错误信息
        /// </summary>
        public string error { set; get; }

        /// <summary>
        /// 请求地址
        /// </summary>
        public string request { set; get; }

    }
}
/*
 * Author: xusion
 * Created: 2012.04.10
 * Support: http://wobumang.com
 */