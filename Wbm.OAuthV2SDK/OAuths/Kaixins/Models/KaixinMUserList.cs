﻿using System;
using System.Collections.Generic;
namespace Wbm.OAuthV2SDK.OAuths.Kaixins.Models
{
    /// <summary>
    /// 实体类MUsers 。
    /// </summary>
    [Serializable]
    public class KaixinMUserList : KaixinMError
    {
        /// <summary>
        /// 用户列表 
        /// </summary>
        public KaixinMUser[] users { set; get; }
    }
}

/*
 * Author: xusion
 * Created: 2012.04.10
 * Support: http://wobumang.com
 */